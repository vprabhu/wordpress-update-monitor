<?php

/**
 * Fired during plugin activation
 *
 * @link       wordpress.org
 * @since      1.0.0
 *
 * @package    Wp_Update_Monitor
 * @subpackage Wp_Update_Monitor/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Update_Monitor
 * @subpackage Wp_Update_Monitor/includes
 * @author     wordpress.org <wordpress.org>
 */
class Wp_Update_Monitor_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
