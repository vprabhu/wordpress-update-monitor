<?php

/**
 * Fired during plugin deactivation
 *
 * @link       wordpress.org
 * @since      1.0.0
 *
 * @package    Wp_Update_Monitor
 * @subpackage Wp_Update_Monitor/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Update_Monitor
 * @subpackage Wp_Update_Monitor/includes
 * @author     wordpress.org <wordpress.org>
 */
class Wp_Update_Monitor_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
